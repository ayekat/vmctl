SRCDIR = src
BUILDDIR = build

PREFIX ?= /usr/local
INSTALLDIR ?= ${PREFIX}

all:
	mkdir -p "${BUILDDIR}"
	sed -e 's|^PREFIX=\.$$|PREFIX="${PREFIX}"|g' <"${SRCDIR}/vmctl" >"${BUILDDIR}/vmctl"
	chmod 755 "${BUILDDIR}/vmctl"
	cp "${SRCDIR}"/*.sh "${BUILDDIR}"/

install:
	install -Dm 755 "${BUILDDIR}"/vmctl "${INSTALLDIR}"/bin/vmctl
	ln -sf vmctl "${INSTALLDIR}/bin/diskctl"
	mkdir -p "${INSTALLDIR}"/lib/vmctl
	install -m 644 "${BUILDDIR}"/*.sh "${INSTALLDIR}"/lib/vmctl
	install -Dm 0644 newvm.cfg "${INSTALLDIR}"/share/doc/vmctl/newvm.cfg
