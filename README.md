vmctl
=====

*vmctl* is a collection of POSIX shell scripts (and the `vmctl` and `diskctl`
commands) to easily run and organise virtual machines with
[Qemu](http://wiki.qemu.org/Main_Page).
