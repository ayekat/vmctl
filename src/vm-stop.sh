description='Stop a running machine'
usage='[OPTIONS ...] MACHINE'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" \
	|| die $E_USER 'Please specify a machine name'
shift
test $# -eq 0 \
	|| die $E_USER "Trailing arguments: $@"

# Read configuration and status:
vm_set "$vm_name"
if [ $vm_state != running ]; then
	die $E_ERROR '%s: Machine is not running' "$vm_name"
fi

# Send powerdown signal:
echo system_powerdown | nc -U "$vm_socketfile" >/dev/null
