VM_CFG_STRICT=$FALSE

vm_exists()
{
	test -e "$VMCTL_MACHINEDIR/${1}.cfg" || return
}

vm_set()
{
	# Default data:
	vm_name="$1"
	vm_machine_description=''
	vm_cpu_type=host
	vm_cpu_cores=1
	vm_cpu_kvm=$TRUE
	vm_gpu_type=cirrus
	vm_memory=1024M
	vm_storage_file=''
	vm_storage_interface=virtio
	vm_storage_format=''
	vm_network_mac=''
	vm_network_type=user
	vm_network_model=virtio
	vm_network_bridge=br0
	vm_control_headless=$FALSE
	vm_control_vnc=''
	vm_options_verbatim=''
	vm_state=unknown
	vm_error=$E_SUCCESS

	vm_cfg="$VMCTL_MACHINEDIR/${vm_name}.cfg"
	vm_rundir="$VMCTL_RUNDIR/$vm_name"
	vm_runcfg="$vm_rundir/cfg"
	vm_cmdfile="$vm_rundir/cmd"
	vm_pidfile="$vm_rundir/pid"
	vm_retvalfile="$vm_rundir/retval"
	vm_socketfile="$vm_rundir/socket"
	vm_tstart=''

	cfg_lineno=0
	section=''

	# Data directory:
	if [ ! -e "$vm_cfg" ]; then
		vm_error '%s: Machine does not exit' "$vm_name"
	elif [ ! -r "$vm_cfg" ]; then
		vm_error '%s: Read permissions denied on configuration' "$vm_name"
	fi
	if [ $vm_error -ne $E_SUCCESS ]; then
		return $vm_error
	fi

	# Runtime directory:
	if [ -d "$vm_rundir" ]; then
		if [ ! -r "$vm_rundir" ]; then
			vm_error $E_PERM '%s: Read permissions denied' "$vm_rundir"
		fi
		if [ $vm_error -ne $E_SUCCESS ]; then
			return $vm_error
		fi

		# State (PID):
		if [ -e "$vm_pidfile" ]; then
			vm_pid="$(cat "$vm_pidfile")"
			if ! test_num "$vm_pid"; then
				die $E_ERROR 'Non-numeric PID `%s` in %s' \
					"$vm_pid" "$vm_pidfile"
			fi
			if ! ps -p $vm_pid >/dev/null; then
				vm_state=lost
			else
				vm_state=running
				vm_tstart="$(ps --no-headers -o lstart -p $vm_pid \
							 | sed -e 's| \+| |g')"
			fi
		else
			vm_state=stopped
		fi
	fi

	# Parse lines:
	while read line; do
		cfg_lineno=$(($cfg_lineno + 1))
		line="$(echo "$line" | cut -d '#' -f 1)"
		test -n "$line" || continue
		case "$line" in
			'') continue ;;
			'['*']') vm_cfg_section "$line" ;;
			*=*) vm_cfg_option "$section" "$line" ;;
			*) vm_error 'Invalid line: %s' "$line" ;;
		esac
		if [ $vm_error -ne $E_SUCCESS -a $VM_CFG_STRICT -eq $TRUE ]; then
			return $vm_error
		fi
	done <"$vm_cfg"
	cfg_lineno=0
	section=''

	# Status (return value):
	if [ -e "$vm_retvalfile" ]; then
		vm_retval="$(cat "$vm_retvalfile")"
		if ! test_num "$vm_retval"; then
			die $E_ERROR 'Non-numeric return value `%s` in %s' \
				"$vm_retval" "$vm_retvalfile"
		fi
		case $vm_retval in
			0) vm_status=success ;;
			*) vm_status=failure ;;
		esac
	else
		vm_status=unknown
	fi
}

vm_cfg_section()
{
	case "$1" in
		'[Machine]') section=machine ;;
		'[CPU]') section=cpu ;;
		'[GPU]') section=gpu ;;
		'[Memory]') section=memory ;;
		'[Storage]') section=storage ;;
		'[Network]') section=network ;;
		'[Control]') section=control ;;
		'[Verbatim]') section=verbatim ;;
		*) vm_error 'Unknown section: %s' "$1"
	esac
}

vm_cfg_option()
{
	section="$1"
	line="$2"
	test -n "$line" || vm_error $E_CFG "Empty line given"
	key="$(echo "$line" | cut -d '=' -f 1)"
	value="$(echo "$line" | cut -d '=' -f 2-)"
	case "$section" in
		machine)
			case "$key" in
				Description) vm_machine_description="$value" ;;
				*) vm_error 'Invalid property: `%s`' "$key"
			esac ;;
		cpu)
			case "$key" in
				Type) vm_cfg_option_cpu_type ;;
				Cores) vm_cfg_option_cpu_cores ;;
				KVM) vm_cfg_option_cpu_kvm ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		gpu)
			case "$key" in
				Type) vm_cfg_option_gpu_type ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		memory)
			case "$key" in
				Memory) vm_memory="$value" ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		storage)
			case "$key" in
				Disk) vm_storage_disk="$value" ;;
				Interface) vm_cfg_option_storage_interface ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		network)
			case "$key" in
				Type) vm_cfg_option_network_type ;;
				MAC) vm_cfg_option_network_mac ;;
				Model) vm_cfg_option_network_model ;;
				Bridge) vm_network_bridge="$value" ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		control)
			case "$key" in
				Headless) vm_cfg_option_control_headless ;;
				VNC) vm_cfg_option_control_vnc ;;
				*) vm_error 'Invalid property: `%s`' "$key" ;;
			esac ;;
		verbatim)
			vm_options_verbatim="$vm_options_verbatim $value" ;;
		*)
			vm_error 'Property without section: `%s`' "$key" ;;
	esac
}

vm_cfg_option_cpu_type()
{
	case "$value" in
		host) vm_cpu_type="$value" ;;
		*) vm_error '[%s] Unknown CPU type: `%s`' "$key" "$value" ;;
	esac
}

vm_cfg_option_cpu_cores()
{
	test_num "$value" && vm_cpu_cores=$value \
		|| vm_error '[%s] Not a numeric value: `%s`' "$key" "$value"
}

vm_cfg_option_cpu_kvm()
{
	test_bool "$value" && vm_cpu_kvm=$(from_bool $value "$vm_cpu_kvm") \
		|| vm_error '[%s] Not a boolean value: `%s`' "$key" "$value"
}

vm_cfg_option_gpu_type()
{
	case "$value" in
		cirrus|std|vmware|qxl|tcx|cg3|virtio) vm_gpu_type="$value" ;;
		*) vm_error '[%s] Unknown GPU type: `%s`' "$key" "$value" ;;
	esac
}

vm_cfg_option_storage_interface()
{
	case "$value" in
		ide|scsi|sd|mtd|floppy|pflash|virtio) vm_storage_interface="$value" ;;
		*) vm_error '[%s] Unknown storage interface: `%s`' "$key" "$value"
	esac
}

vm_cfg_option_network_type()
{
	case "$value" in
		user|bridge) vm_network_type="$value" ;;
		''|none) vm_network_type='' ;;
		*) vm_error '[%s] Unknown network type: `%s`' "$key" "$value" ;;
	esac
}

vm_cfg_option_network_model()
{
	case "$value" in
		virtio|i82551|i82557b|i82559er|ne2k_pci|ne2k_isa|pcnet|rtl8139|e1000|smc91c111|lance|mcf_fec)
			vm_network_model="$value" ;;
		*) vm_error '[%s] Unknown NIC model: `%s`' "$key" "$value" ;;
	esac
}

vm_cfg_option_network_mac()
{
	case "$value" in
		52:54:??:??:??:??)
			vm_network_mac="$value" ;;
		??:??:??:??:??:??)
			vm_error '[%s] MAC address must start with 52:54:' "$key" ;;
		*)
			vm_error '[%s] Invalid MAC address: `%s`' "$key" "$value" ;;
	esac
}

vm_cfg_option_control_headless()
{
	test_bool "$value" \
		&& vm_control_headless=$(from_bool $value "$vm_control_headless") \
		|| vm_error '[%s] Not a boolean value: `%s`' "$key" "$value"
}

vm_cfg_option_control_vnc()
{
	test_num "$value" && vm_control_vnc=$value \
		|| vm_error '[%s] Not a numeric value: `%s`' "$key" "$value"
}

vm_error()
{
	format="$1"
	shift
	if [ $cfg_lineno -gt 0 ]; then
		error "%s:%d: $format" "$(basename "$vm_cfg")" $cfg_lineno "$@"
		retval=$E_CFG
	else
		error "$format" "$@"
		retval=$E_USER
	fi
	vm_error=$retval
	if [ $VM_CFG_STRICT -eq $TRUE ]; then
		exit $vm_error
	fi
}

vm_set_pid()
{
	vm_create_rundir

	vm_pid=$1
	if [ ! -w "$vm_rundir" ]; then
		die $E_PERM '%s: Write permissions denied' "$vm_rundir"
	fi
	if [ $vm_pid -eq 0 ]; then
		rm -f "$vm_pidfile"
	else
		echo $vm_pid >"$vm_pidfile"
	fi
}

vm_set_retval()
{
	vm_create_rundir

	vm_retval=$1
	if [ ! -w "$vm_rundir" ]; then
		die $E_PERM '%s: Write permissions denied' "$vm_rundir"
	fi
	echo $vm_retval >"$vm_retvalfile"
}

vm_create_rundir()
{
	if ! mkdir -p "$vm_rundir"; then
		die $E_PERM '%s: Could not create directory' "$vm_rundir"
	fi
}
