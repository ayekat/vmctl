description='Create a new disk image'
usage='[OPTIONS ...] NAME SIZE'
options()
{
	cat <<- EOF
	  -h, --help         Display this message and exit
	  -f, --format FMT   Use format FMT for the new disk [default=$disk_format]
	EOF
}
remark='Run with `--format help` to list all available formats.'
help_format()
{
	cat <<- EOF
	Available disk formats:
	  raw          Raw disk image
	  qcow2        QEMU Copy-On-Write (version 2)
	EOF
}

disk_format=raw
while [ $# -gt 0 ]; do
	case "$1" in
		-f|--format) shift; disk_format="$1" ;;
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Check arguments:
case "$disk_format" in
	raw|qcow2) : ;;
	help) help_format; exit $E_SUCCESS ;;
	*) die $E_USER 'Invalid disk format: %s' "$disk_format" ;;
esac

# Read disk name/size:
disk_name="$1"
test -n "$disk_name" || die $E_USER 'Please specify a disk name'
shift
disk_size="$1"
test -n "$disk_size" || die $E_USER 'Please specify a disk size'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

# Create new disk:
disk_file="$VMCTL_DISKDIR/${disk_name}.${disk_format}"
if [ -e "$disk_file" ]; then
	die $E_ERROR '%s: Disk already exists (%s)' "$disk_name" "$disk_file"
fi
mkdir -p "$VMCTL_DISKDIR"
qemu-img create -f "$disk_format" "$disk_file" "$disk_size"
"$0" status "$disk_name"
