description='Create a new machine'
usage='[OPTIONS ...] MACHINE'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" || die $E_USER 'Please specify a machine name'
shift
case $vm_name in (.*) die $E_USER 'Invalid machine name: %s' "$vm_name" ;; esac
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

# Create configuration file:
if [ -e "$VMCTL_MACHINEDIR/${vm_name}.cfg" ]; then
	die $E_ERROR '%s: Machine already exists' "$vm_name"
fi
mkdir -p "$VMCTL_MACHINEDIR"
cp "$PREFIX"/share/doc/vmctl/newvm.cfg "$VMCTL_MACHINEDIR"/"$vm_name".cfg
"$0" status "$vm_name"
