description='Display status information for a machine'
usage='[OPTIONS ...] MACHINE'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" || die $E_USER 'Please specify a machine name'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

vm_set "$vm_name"

# Basic information (name, description):
printf "\033[1m%s\033[0m" "$vm_name"
test -z "$vm_machine_description" || printf " - %s" "$vm_machine_description"
printf "\n"

# Status:
case $vm_state in
	unknown|stopped) vm_status_colour='0' ;;
	running) vm_status_colour='32;1' ;;
	lost) vm_status_colour='31;1' ;;
esac
case $vm_status in
	success) : ;;
	failure)
		case $vm_state in (stopped|lost)
			vm_state=failed
			vm_status_colour='31;1'
		esac ;;
esac
printf "  State: \033[%sm%s\033[0m" "$vm_status_colour" "$vm_state"
case $vm_state in
	running|lost)
		printf " (PID: %d" $vm_pid
		if [ $vm_state = running ]; then
			printf ", since %s" "$vm_tstart"
		fi
		printf ")";;
	failed|stopped)
		printf " (status: %d)" $vm_retval ;;
esac
printf "\n"

# Runtime information:
if [ $vm_state = running ]; then
	printf "  Socket: %s\n" "$vm_socketfile"
	printf "  Headless: %s" $(strbool_yesno $vm_control_headless)
	if [ $vm_control_headless -eq $TRUE ]; then
		printf " (VNC: %s)" "$(strmaybe "$vm_control_vnc")"
	fi
	printf "\n"

	printf "  CPU: %s (%d core%s%s), " \
		"$vm_cpu_type" $vm_cpu_cores "$(test $vm_cpu_cores -eq 1 || echo s)" \
		"$(test $vm_cpu_kvm -eq $FALSE || echo ", KVM")"
	printf "Memory: %s, " "$vm_memory"
	printf "Network: %s" "$(strmaybe "$vm_network_type")"
	case "$vm_network_type" in
		bridge) printf " (%s)" "$vm_network_bridge" ;;
	esac
	if [ -n "$vm_network_mac" ]; then
		printf ", MAC: %s" "$vm_network_mac"
	fi
	printf "\n"
fi
