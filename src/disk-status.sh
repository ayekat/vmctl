description='Display informations about a disk image'
usage='[OPTIONS ...] DISK'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read disk name:
vm_name="$1"
test -n "$vm_name" || die $E_USER 'Please specify a disk name'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

disk_set "$vm_name"

# Basic information (name, size):
printf "\033[1m%s\033[0m.%s\n" "$disk_name" "$disk_format"
printf "  Size: %s\n" "$(size_human $disk_size)"

# Disk used by ...
usedby=''
for vm_cfg in "$VMCTL_MACHINEDIR"/*.cfg; do
	vm_name="$(basename "$vm_cfg" .cfg)"
	vm_set "$vm_name" || continue
	if [ "$vm_storage_disk" = "$disk_name" ]; then
		usedby="$usedby$vm_name "
	fi
done
if [ -n "$usedby" ]; then
	printf "  Used by: %s\n" "$usedby"
fi
