description='List available disk images'
usage='[OPTIONS ...]'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) die $E_USER "Trailing arguments: $@" ;;
	esac
	shift
done

# List disks:
for disk_file in "$VMCTL_DISKDIR"/*; do
	test -e "$disk_file" || continue   # when globbing * in an empty directory

	disk_set "$(basename "${disk_file%.*}")" || continue
	printf "\033[1m%s\033[0m.%s (%s)\n" \
		"$disk_name" "$disk_format" "$(size_human "$disk_size")"
done
