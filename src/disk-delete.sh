description='Delete an existing disk image'
usage='[OPTIONS ...] DISK'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read disk name:
disk_name="$1"
test -n "$disk_name" || die $E_USER 'Please specify a disk image name'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

# Determine whether we can remove the disk:
usedby=''
for vm_name in "$VMCTL_MACHINEDIR"/*; do
	test -e "${vm_name}.cfg" || continue
	vm_set "$vm_name"
	if [ "$vm_disk" = "$disk_name" ]; then
		usedby="$usedby$vm_name "
	fi
done
test -z "$usedby" || die $E_ERROR '%s: Used by %s' "$disk_name" "$usedby"

# Remove disk:
disk_set "$disk_name"
rm "$disk_file"
