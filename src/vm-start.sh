description='Start a machine'
usage='[OPTIONS ...] MACHINE'
options() {
	cat <<- EOF
	  -h, --help       Display this message and exit
	  -i, --iso FILE   Boot from ISO disk image file FILE
	  -n, --net        Boot from network (PXE)
	EOF
}

bootnet=$FALSE
while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-i|--iso) shift; iso_file="$1" ;;
		-n|--net) bootnet=$TRUE ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" || die $E_USER 'Please specify a machine name'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $*"

# Read configuration/state:
VM_CFG_STRICT=$TRUE
vm_set "$vm_name"
if [ $vm_state = running ]; then
	die $E_ERROR '%s: Machine is already running' "$vm_name"
fi

# Check storage:
if [ -z "$vm_storage_disk" ]; then
	if [ -z "$iso_file" ]; then
		die $E_CFG '%s: No storage file specified' "$vm_name"
	fi
else
	disk_set "$vm_storage_disk"
	if [ ! -r "$disk_file" ] || [ ! -w "$disk_file" ]; then
		die $E_PERM '%s: Permissions denied: %s' "$vm_name" "$disk_file"
	fi
fi

# Initialise PID file and runtime configuration:
vm_set_pid $$
cp "$vm_cfg" "$vm_rundir/cfg"

# CPU:
test $vm_cpu_kvm -eq $FALSE || opt_kvm='-enable-kvm'
opt_cpu="-cpu $vm_cpu_type -smp $vm_cpu_cores $opt_kvm"

# Network:
opt_nic=
opt_net=
if [ -n "$vm_network_type" ]; then
	opt_nic="-net nic,model=$vm_network_model"
	test -z "$vm_network_mac" || opt_nic="$opt_nic,macaddr=$vm_network_mac"
	opt_net="-net $vm_network_type"
	case "$vm_network_type" in
		bridge) opt_net="$opt_net,br=$vm_network_bridge" ;;
	esac
fi

# Storage:
if [ -n "$disk_file" ]; then
	opt_storage="-drive file=\"$disk_file\",if=$vm_storage_interface,format=$disk_format"
fi
if [ -n "$iso_file" ]; then
	iso_file="$(readlink -f "$iso_file")"
	test -e "$iso_file" || die $E_ERROR '%s: File not found' "$iso_file"
	test -r "$iso_file" || die $E_PERM '%s: Permission denied' "$iso_file"
	opt_iso="-drive file=\"$iso_file\",media=cdrom -boot order=d"
elif [ $bootnet -eq $TRUE ]; then
	opt_iso='-boot order=n'
else
	opt_iso=''
fi

# Control (headless/VNC):
if [ $vm_control_headless -eq $TRUE ]; then
	if [ -n "$vm_control_vnc" ]; then
		opt_vnc="-vnc :$vm_control_vnc"
	else
		warn 'Starting headless machine without VNC'
		opt_vga='-nographic'
	fi
else
	opt_vga="-vga $vm_gpu_type"
fi

# Launch VM:
qemu_launch()
{
	cat >"$vm_cmdfile" <<- EOF
		exec qemu-system-x86_64 \\
		    $opt_cpu \\
		    -m $vm_memory \\
		    $opt_nic $opt_net \\
		    $opt_storage $opt_iso \\
		    -usb \\
		    $opt_vga \\
		    $opt_vnc -monitor unix:"$vm_socketfile",server,nowait \\
		    $vm_options_verbatim
	EOF
	chmod 755 "$vm_cmdfile"
	exec "$vm_cmdfile" &
	vm_pid=$!
	vm_set_pid $vm_pid
	set +e
	wait $vm_pid
	vm_set_retval $?
	vm_set_pid 0
	set -e
}
qemu_launch &
