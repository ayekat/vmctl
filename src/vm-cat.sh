description='Dump configuration for a machine to stdout'
usage='[OPTIONS ...] MACHINE'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" \
	|| die $E_USER 'Please specify a machine name'
shift
test $# -eq 0 \
	|| die $E_USER "Trailing arguments: $@"

# Dump configuration:
vm_set "$vm_name"
more "$vm_cfg"
